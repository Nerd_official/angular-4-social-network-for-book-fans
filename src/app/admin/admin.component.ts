import { Component, OnInit } from '@angular/core';
import {Books} from '../Books';
import {PassDataService} from '../service/pass-data.service';
import {BookService} from '../service/book.service';
import {Router} from '@angular/router';
import {User} from '../User';
import {Comment} from '../Comment';
import {Author} from '../Author';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],
  providers: [BookService]
})
export class AdminComponent implements OnInit {
  books: Books;
  booksArray: Array<Books>;
  addAuthorForm: boolean;
  showBookPost: boolean;
  showUsersPost: boolean;
  showAdminBooks = true;
  showAdminUsers: boolean;
  showAdminAuthors: boolean;
  showAdminComments: boolean;
  usersArray: Array<User>;
  comments: Array<Comment>;
  authors: Array<Author>;
  formData = new FormData();
  constructor(private passDataService: PassDataService, private _bookService: BookService, private __router: Router) { }
  addBook() {
    this.showBookPost = true;
  }
  ngOnInit() {
    if (this.showAdminBooks) {
      this.getBooks();
    }
  }
  // Get all books
  getBooks() {
    this._bookService.getBooks()
      .subscribe(
        res => {
          this.booksArray = res;
    });
  }
  // Update a book
  updateBook(book) {
    this._bookService.updateBooks(book)
      .subscribe(
        res => {
          this.booksArray = res;
    });
  }
  // Post book data
  onSubmitBookPost(book: Books) {
    // Append firstname and lastname to formdata
    this.formData.set('lastName', book.lastName.toString());
    this.formData.set('firstName', book.firstName.toString());
    if (book.title !== '') {
      this.formData.set('title', book.title.toString());
    }
    this._bookService.saveAdminBook(this.formData)
      .subscribe(resNewBook => {
        this.showBookPost = false;
        this.getBooks();
    });
  }
  // Create a new user
  onSubmitUserPost(form) {
    this._bookService.saveAdminUser(form)
      .subscribe(res => {
        this.showUsersPost = false;
        this.showUsers();
    });
  }
  // Update user data
  onSubmitUpdateUserPost(form, user) {
    this._bookService.updateAdminUser(form, user)
      .subscribe(res => {
    });
  }
  // Update user comments
  onSubmitUpdateAuthors(form, author) {
    this._bookService.updateAuthor(form, author)
      .subscribe(res => {
    });
  }
  // Update user comments
  onSubmitUpdateUserComments(form, comment) {
    this._bookService.updateUserComments(form, comment)
      .subscribe(res => {
    });
  }
  // Create a new author
  onSubmitAuthor(author: Author) {
    let formData: FormData = new FormData();
    if (author.firstName !== '' && author.lastName !== '') {
      formData.set('firstName', author.firstName.toString());
      formData.set('lastName', author.lastName.toString());
    }
    this._bookService.saveAuthor(formData)
      .subscribe(res => {
        this.authors.push(res);
        this.addAuthorForm = false;
    });
  }
  // Get image and save file name to form data
  onFileChange(event) {
    const image: FileList = event.target.files;
    let file: File = image[0];
    this.formData.set('uploads', file, file.name);
  }
  // Delete a book
  deleteBook(book: Books) {
    this.showBookPost = false;
    this._bookService.deleteBook(book)
      .subscribe(res => {
        this.booksArray = this.removeElement(this.booksArray, book);
    });
  }
  // Delete authors
  deleteAuthor(author: Author) {
    const authorArray = this.authors;
    this._bookService.deleteAuthor(author)
      .subscribe(res => {
        this.authors = this.removeElement(this.authors, author);
    });
  }
  // Delete comments
  deleteComment(comment: Comment) {
    this._bookService.deleteComment(comment)
      .subscribe(res => {
        this.comments = this.removeElement(this.comments, comment);
      });
  }
  // Delete users
  deleteUser(user: User) {
    this._bookService.deleteUser(user)
      .subscribe(res => {
       this.usersArray = this.removeElement(this.usersArray, user);
    });
  }
  // Remove element from array
  removeElement(data, element) {
    for (let i = 0; i < data.length; i++) {
      if (data[i]._id ===  element._id) {
          data.splice(i, 1);
      }
    }
    return data;
  }
  addUsers() {
    this.showUsersPost = true;
  }
  // Display all books
  showBooks() {
    this.showAdminBooks = true;
    this.showAdminUsers = false;
    this.showAdminComments = false;
    this.showAdminAuthors = false;
  }
  // Dispaly all users
  showUsers() {
    this._bookService.getAdminUsers()
      .subscribe(res => {
        this.usersArray = res;
    });
    this.showAdminUsers = true;
    this.showAdminBooks = false;
    this.showAdminComments = false;
    this.showAdminAuthors = false;
  }
  // Display all comments
  showComments() {
    this._bookService.getAllComments()
      .subscribe(res => {
        this.comments = res;
    });
    this.showAdminComments = true;
    this.showAdminUsers = false;
    this.showAdminBooks = false;
    this.showAdminAuthors = false;
  }
  // Display all authors
  showAuthors() {
    this._bookService.getAuthors()
      .subscribe(res => {
        this.authors = res;
    });
    this.showAdminAuthors = true;
    this.showAdminComments = false;
    this.showAdminUsers = false;
    this.showAdminBooks = false;
  }
  addAuthors() {
    this.addAuthorForm = true;
  }
  // Cancel authors form
  cancelAddAuthor() {
    this.addAuthorForm = false;
  }
  // Cancel users form
  cancelAddUsers() {
    this.showUsersPost = false;
  }
  // Cancel books form
  cancelAddBooks() {
    this.showBookPost = false;
  }
}
