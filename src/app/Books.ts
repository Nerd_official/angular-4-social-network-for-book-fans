export class Books {
  _id?: string;
  mypath: String;
  author: String;
  title: String;
  url: String;
  books: String;
  lastName: String;
  firstName: String;
  comments: String;
  favourite: String;
  rating: Number;
  checked: String;
  checked1: String;
  checked2: String;
  checked3: String;
  checked4: String;
}

