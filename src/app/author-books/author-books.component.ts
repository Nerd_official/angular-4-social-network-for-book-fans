import { Component, OnInit } from '@angular/core';
import {Books} from '../Books';
import {Router} from '@angular/router';
import {BookService} from '../service/book.service';
import {Author} from "../Author";
import {PassDataService} from "../service/pass-data.service";
import {Subscription} from "rxjs/Subscription";
import {AuthService} from "../service/auth.service";

@Component({
  selector: 'app-author-books',
  templateUrl: './author-books.component.html',
  styleUrls: ['./author-books.component.css'],
  providers: [BookService]
})
export class AuthorBooksComponent implements OnInit {
  get subscription(): Subscription {
    return this._subscription;
  }

  set subscription(value: Subscription) {
    this._subscription = value;
  }

  bookPost: Array<Books>;
  header: String;
  private _subscription: Subscription;
  author: Author;
  showBookPost = false;
  isLoggedIn = false;
  fName: String;
  lName: String;
  id: any;
  formData = new FormData();
  constructor(private _bookService: BookService, private router: Router, private passDataService: PassDataService
    , private auth: AuthService) {
  }
  ngOnInit() {

    // Store author object in local storage for page refresh
    this.passDataService.message.subscribe(message =>  {if (typeof message === 'object') {
      this.author = (message);
      localStorage.object = JSON.stringify( message);
    } else {
      this.author = JSON.parse(localStorage.object);
    } });
    // Get author first and last name
    this.fName = this.author.firstName;
    this.lName = this.author.lastName;
    // Check login status
    this.auth.isAuthenticated().subscribe(result => {
        if (result === true) {
          this.isLoggedIn = true;
        } else {
          this.isLoggedIn = false;
        }
    });

    this.getAuthorsBooks();
    this.header = this.fName + " " + this.lName;
  }
  // Save book details
  saveValue(val) {
    this._bookService.saveBook(this.formData, val)
      .subscribe(resNewBook => {
        this.showBookPost = false;
        this.getAuthorsBooks();
    });
  }
  // Show form when add book is clicked
  addBook() {
    this.showBookPost = true;
  }
  // Hide form when the cancel button is clicked
  cancelPost() {
    this.showBookPost = false;
  }
  // Post book data
  onSubmitPost(book: Books) {
    this.id = localStorage.getItem('path');
    // Append firstname and lastname to formdata
    this.formData.set('lastName', this.lName.toString());
    this.formData.set('firstName', this.fName.toString());
    if (book.title !== '') {
      this.formData.set('title', book.title.toString());
    }
    // Append book id
    this.formData.set('path', this.id);
    this._bookService.saveBook(this.formData, this.author)
      .subscribe(resNewBook => {
        this.showBookPost = false;
        this.getAuthorsBooks();
      });
  }
  // Get image and save file name to form data
  onFileChange(event) {
    console.log('Fileupload' + event.target.files[0].name);
    const image: FileList = event.target.files;
    let file: File = image[0];
    this.formData.set('uploads', file, file.name);
  }
  // Get all authors books
  getAuthorsBooks() {
    const id = JSON.parse(localStorage.getItem('authorId'));
    let formData: FormData = new FormData();

    formData.append('path', id);
    this._bookService.getAuthorsBooks(formData)
      .subscribe(resNewBook => {
        this.bookPost = resNewBook;
      });
  }
  // Pass book details to show component
  singleBook(event) {
    this.passDataService.sendObject(event);
    this.router.navigate(['/comment']);
  }
}
