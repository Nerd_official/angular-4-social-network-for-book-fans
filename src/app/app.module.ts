import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RatingsComponent} from './ratings/ratings.component';
import { ShowComponent } from './show/show.component';
import {AuthService} from './service/auth.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { LoginComponent } from './login/login.component';
import { CommentComponent } from './comment/comment.component';
import { AuthorComponent } from './author/author.component';
import {PassDataService} from "./service/pass-data.service";
import { RegisterComponent } from './register/register.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { AuthorBooksComponent } from './author-books/author-books.component';
import { AdminComponent } from './admin/admin.component';
import {AdminGuardService} from './service/admin-guard.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RatingsComponent,
    ShowComponent,
    LoginComponent,
    CommentComponent,
    AuthorComponent,
    RegisterComponent,
    UserProfileComponent,
    AuthorBooksComponent,
    AdminComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
  ],
  providers: [AuthService, JwtHelperService, PassDataService, AdminGuardService],
  bootstrap: [AppComponent]
})

export class AppModule { }

