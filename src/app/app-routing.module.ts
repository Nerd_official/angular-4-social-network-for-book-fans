import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate  } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {RatingsComponent} from './ratings/ratings.component';
import {ShowComponent} from "./show/show.component";
import {LoginComponent} from "./login/login.component";
import {AuthorComponent} from "./author/author.component";
import {RegisterComponent} from "./register/register.component";
import {UserProfileComponent} from './user-profile/user-profile.component';
import {AuthorBooksComponent} from './author-books/author-books.component';
import {AdminComponent} from './admin/admin.component';
import {AdminGuardService} from './service/admin-guard.service';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'author',
    component: AuthorComponent
  },
  {
    path: 'author-books',
    component: AuthorBooksComponent
  },
  {
    path: 'update',
    component: RatingsComponent
  },
  {
    path: 'comment',
    component: ShowComponent
  },
  {
    path: 'user-profile',
    component: UserProfileComponent
  },
  {
    path: 'admin',
    component: AdminComponent, canActivate: [AdminGuardService]
  },
  {
    path: '**',
    component: HomeComponent
  },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
