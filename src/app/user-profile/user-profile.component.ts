import { Component, OnInit } from '@angular/core';
import {BookService} from '../service/book.service';
import {PassDataService} from '../service/pass-data.service';
import {Router} from '@angular/router';
import * as _ from 'lodash';
import {User} from '../User';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css'],
  providers: [BookService]
})
export class UserProfileComponent implements OnInit {

  constructor(private passDataService: PassDataService, private _bookService: BookService, private __router: Router) { }
  username: any;
  test = [];
  userFavouriteBooks = [];
  isLoggedIn = true;
  id: any;
  count: number;
  mySet = new Set();
  j = 0;
  ngOnInit() {
    this.count = 0;
    // Get the user object
    this.passDataService.user.subscribe(message =>  {if (typeof message === 'object') {
      this.mySet = new Set();
      this.username = message.userName;
      this.id = message._id;
      // Set data for page refresh
      localStorage.setItem('user-profileId', message._id);
      localStorage.setItem('user-profile-username', message.userName);
    } else {
      // Get data after Page refresh
      this.username = localStorage.getItem('user-profile-username');
      this.id = localStorage.getItem('user-profileId');
    }
      this.getCurrentUserFavouriteBooks(message);
    });
  }
  // Pass the book data
  sendBook(event) {
    this.passDataService.sendObject(event);
    this.__router.navigate(['/comment']);
  }
// Check database for the current user favourite books
  getCurrentUserFavouriteBooks(message) {
    this._bookService.getCurrentUserFavouriteBooks(message)
      .subscribe(result => {
        for (const property in result) {
          if (result.hasOwnProperty(property)) {
            this.userFavouriteBooks = result[property].favourites;
          }
        }
        // Get the ids of all favourite book users
        let favBooksId = [];
        var i = 0;
        result[i].favourites.forEach(function(element) {
          favBooksId.push(element._id);
          this.getAllFavouriteUsers(favBooksId[i]);
          i++;
        }.bind(this));
      });
  }
  // Get the users who have favourited the same books
  getAllFavouriteUsers(data) {
    this._bookService.getAllFavouriteUsers(data).subscribe(res => {
      let i = 0;
      res[i].favourite.forEach(function(element) {
        if (element._id !== this.id) {
          this.mySet.add(element.userName);
        }
        i++;
      }.bind(this));

    });

  }
  // Pass the clicked user data
  userProfile(user) {
    this._bookService.getUserId(user)
      .subscribe(res => {
        let newUser = new User();
        newUser._id = res[0]._id;
        newUser.userName = res[0].userName;
        this.passDataService.sendUser(newUser);
        this.__router.navigate(['/user-profile']);
    });
  }
}
