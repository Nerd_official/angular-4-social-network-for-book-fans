import { Component, OnInit } from '@angular/core';
import {BookService} from '../service/book.service';
import {Router} from '@angular/router';
import {AuthService} from '../service/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  message: string;
  registerForm = true;
  constructor(private authService: AuthService, private __router: Router) { }
  errors: [{
    email: string,
    password: string,
    repeatPassword: string,
    username: string,
    passwordCheck: string
  }];
  ngOnInit() {
  }
  // Submit button clicked
  onSubmitRegister (event: any) {
    const error = new Errors();
    let isValidCount = 5;
    if (event.value.username === '') {
      error.username = "*Please enter your Username";
      isValidCount --;
    }
    if (event.value.email === '') {
      error.email = "*Please enter your Email Address";
      isValidCount --;
    }
    if (event.value.password === '') {
      error.password = "*Please enter your Password";
      isValidCount --;
    }
    if (event.value.password === '' || event.value.rPassword === '') {
      error.repeatPassword = "*Please repeat your Password";
      isValidCount --;
    } else if (event.value.password !== event.value.rPassword ) {
      error.passwordCheck = "*Your Passwords do not match";
      isValidCount --;
    }
    this.errors = [{email: error.email,
      password: error.password,
      repeatPassword: error.repeatPassword,
      username: error.username,
      passwordCheck: error.passwordCheck}];
    if (isValidCount === 5) {
      this.registerForm = false;
      this.authService.saveUserRegisterDetails(event.value).subscribe(res => {
         this.message = res.msg;
      });
    }
  }
}
export class Errors {
  _id?: string;
  email: string;
  password: string;
  repeatPassword: string;
  username: string;
  passwordCheck: string;
}
