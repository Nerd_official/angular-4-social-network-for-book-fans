import {Component, EventEmitter, Input, OnInit} from '@angular/core';
import {Comment} from '../Comment';
import {Router} from "@angular/router";
import {AuthService} from "../service/auth.service";
import {BookService} from "../service/book.service";
import {PassDataService} from "../service/pass-data.service";
import {Books} from '../Books';
@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {
  isLoggedIn: boolean;
  showRating = false;
  showCommentForm = false;
  @Input() postId;
  post: Books;

  constructor(private passDataService: PassDataService, private authService: AuthService, private __router: Router,  private _bookService: BookService) { }
  ngOnInit() {
    // Check login status
    this.isAuthenticated();
    // Get the book object
    this.passDataService.message.subscribe(message => this.post = (message));
    this.showRating = false;
  }
  // Check if user is authenticated
  isAuthenticated() {
    this.authService.isAuthenticated()
      .subscribe(result => {
        this.isLoggedIn = result;
    });
  }
  // Submit and save comment
  onSubmitComment(form) {
    this.showCommentForm = false;
    form.name = localStorage.getItem('username');
    this._bookService.saveComment(form, this.post)
      .subscribe(resNewBook => {
        this.passDataService.sendComment(true);
    });
  }
  // If a user is logged in the show the form
  comment() {
    if (this.isLoggedIn) {
      this.showCommentForm = true;
    } else {
      this.__router.navigateByUrl('/login');
    }
  }
  // If a user is logged in then show the ratings
  rating() {
    if (this.isLoggedIn) {
      this.showRating = true;
    } else {
      this.__router.navigateByUrl('/login');
    }
  }
}
