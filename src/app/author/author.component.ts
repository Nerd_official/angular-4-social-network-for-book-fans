import { Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Component, Input, OnInit} from '@angular/core';
import {BookService} from '../service/book.service';
import {Router} from '@angular/router';
import {Author} from '../Author';
import {PassDataService} from "../service/pass-data.service";
import {AuthService} from "../service/auth.service";

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.css'],
  providers: [BookService]
})
export class AuthorComponent implements OnInit {
  constructor(private passDataService: PassDataService, private auth: AuthService, private http: Http,
              private _bookService: BookService, private router: Router) { }
  formData: FormData;
  addAuthor = false;
  author: Array<Author>;
  addBook = false;
  ngOnInit() {
    // Check if user is logged in
    this.auth.isAuthenticated()
      .subscribe(result => {
        if (result === true) {
          this.addAuthor = true;
        } else {
          this.addAuthor = false;
        }
    });
    // Get authors
    this._bookService.getAuthors()
      .subscribe(res => {
        this.author = res;
      });
  }
  // Show/remove add book button
  addAuthorBook() {
       this.addBook = true;
  }
  // Hide add book form
  cancelAdd() {
    this.addBook = false;
  }
 // Submit and save new author details
  onSubmitAuthor(author: Author) {
    this.addBook = false;
    let formData: FormData = new FormData();
    if (author.firstName !== '' && author.lastName !== '') {
      formData.append('firstName', author.firstName.toString());
      formData.append('lastName', author.lastName.toString());
    }
    this.formData = formData;
    this._bookService.saveAuthor(this.formData)
      .subscribe(res => { this.author.push(res);
    });
  }
  // Select author
  chooseAuthor(e) {
    this.passDataService.sendObject(e);
    this.router.navigate(['/author-books']);
    localStorage.setItem('authorId', JSON.stringify(e._id.toString()));
  }
}
