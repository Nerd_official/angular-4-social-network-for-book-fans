import {AfterContentInit, AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {PassDataService} from './service/pass-data.service';
import {Router} from '@angular/router';
import {User} from './User';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  token: string;
  isLoggedIn: boolean;
  isAdmin: boolean;
  constructor( private __router: Router, private passDataService: PassDataService) {this.isLoggedIn = true; }
  ngOnInit() {
    // Receive login in status
    this.passDataService.admin.subscribe(message => {
      // Store in local storage for page refresh
      if (typeof message === 'object') {
        for (var i in message) {
          this.isLoggedIn = message[i].login;
          this.isAdmin = message[i].admin;
          localStorage.setItem('bool-login', String(message[i].login));
          localStorage.setItem('bool-admin', String(message[i].admin));
        }
      } else {
          this.isLoggedIn = localStorage.getItem('bool-login') === 'true';
          this.isAdmin = localStorage.getItem('bool-admin') === 'true';
      }
    });
  }
  // Send to log in component
  checkIn() {
    this.__router.navigateByUrl('login');

  }
  // Log out and clear local storage
  clear() {
    localStorage.removeItem('token');
    localStorage.clear();
    this.passDataService.sendAdmin([{login: false, admin: false}]);
  }
  // Send current user data to user profile component
  userProfile() {
    const username = localStorage.getItem('username');
    const id = localStorage.getItem('id');
    const user = new User();
    user._id = id;
    user.userName = username;
    this.passDataService.sendUser(user);
  }
}
