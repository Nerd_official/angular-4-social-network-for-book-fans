import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Books} from '../Books';
import {PassDataService} from '../service/pass-data.service';
import {BookService} from '../service/book.service';

@Component({
  selector: 'app-ratings',
  templateUrl: './ratings.component.html',
  styleUrls: ['./ratings.component.css']
})

export class RatingsComponent implements OnInit {
  found: any;
  @Input() postUpdate;

  ratings = [];
  id: string;
  checked: string;
  checked1: string;
  checked2: string;
  checked3: string;
  checked4: string;
  @Output() onRatingCall = new EventEmitter();
  @Input() startRating;
  post: Books;
  newRating: boolean;
  constructor(private passDataService: PassDataService,  private _bookService: BookService) { }
  ngOnInit() {
    // Get the book object
    this.passDataService.message.subscribe(message => this.post = (message));
    this.id = this.post._id;
    this.onRatingCall.emit(this.post);
    this.getBookRating();
  }
  // Display ratings
  getRating(val, newRate: boolean) {
    if (val === 1) {
      this.checked = 'checked';
      this.checked1 = '';
      this.checked2 = '';
      this.checked3 = '';
      this.checked4 = '';
    }
    if (val === 2) {
      this.checked = 'checked';
      this.checked1 = 'checked';
      this.checked2 = '';
      this.checked3 = '';
      this.checked4 = '';
    }
    if (val === 3) {
      this.checked = 'checked';
      this.checked1 = 'checked';
      this.checked2 = 'checked';
      this.checked3 = '';
      this.checked4 = '';
    }
    if (val === 4) {
      this.checked = 'checked';
      this.checked1 = 'checked';
      this.checked2 = 'checked';
      this.checked3 = 'checked';
      this.checked4 = '';
    }
    if (val === 5) {
      this.checked = 'checked';
      this.checked1 = 'checked';
      this.checked2 = 'checked';
      this.checked3 = 'checked';
      this.checked4 = 'checked';
    }
    this.saveBookRating(val, newRate);
  }
  // Save new ratings
  saveBookRating(val: number, newRate) {
    if (val >= 1 && newRate) {
      this._bookService.saveRating(val, this.post)
        .subscribe(result => {
      });
    }
  }
  // Get the rating made by the user for current book
  getBookRating() {
    this._bookService.getBookRating(this.post)
      .subscribe(result => {
       for (const r of result){
          this.ratings = r.rating;
        }
        const bookId = this.post._id;
        let rate = 0;
        // Check if the bookId is in the ratings array
        this.ratings.forEach(function(element) {
          if (element.bookId === bookId) {
            rate = element.rating;
          }
        });
        if (rate > 0) {
          this.getRating(rate, false);
        }
    });
  }

}
