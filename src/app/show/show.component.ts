import {Component, Input, OnInit} from '@angular/core';
import {Comment} from "../Comment";
import {Books} from "../Books";
import { Location } from '@angular/common';
import {BookService} from "../service/book.service";
import {Router} from "@angular/router";
import {AuthService} from "../service/auth.service";
import {PassDataService} from "../service/pass-data.service";

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css'],
  providers: [BookService]
})
export class ShowComponent implements OnInit {
  post: Books;
  comments: Comment;
  favouriteUser = [];
  isLoggedIn: boolean;
  isFavourite = false;
  constructor(private passDataService: PassDataService, private authService: AuthService, private location: Location,
              private _bookService: BookService, private __router: Router) { }
  ngOnInit() {
    // Check login status
    this.isAuthenticated();
    // Get the book object and store in local storage for page refresh
    this.passDataService.message.subscribe(message =>  {if (typeof message === 'object') {
      this.post = (message);
      localStorage.object = JSON.stringify(message);
    }
    });
    this.getComments();
    // Get notified when a comment was posted
    this.passDataService.comment.subscribe(result => this.getComments());
  }
  // Check if user is authenticated
  isAuthenticated() {
    this.authService.isAuthenticated()
      .subscribe(result => {
        this.isLoggedIn = result;
        if (this.isLoggedIn) {
          this.isFavouriteBook();
        }
    });
  }
  // Get all comments for current book
  getComments() {
    this.post = JSON.parse(localStorage.object);
    this._bookService.getComments(this.post)
      .subscribe(resNewBook => {
        this.comments = resNewBook;
      });
  }
  // Add favourite book
  addFavourite() {
    this.isFavourite = true;
    this._bookService.saveFavourite(this.post)
      .subscribe(resNewBook => {
      });
  }
  // Remove favourite book
  removeFavourite() {
    this.isFavourite = false;
    this._bookService.removeFavourite(this.post)
      .subscribe(resNewBook => {
      });
  }
  // Check database to see if current book is the users favourite book
  isFavouriteBook() {
    this._bookService.isFavouriteBook(this.post)
      .subscribe(resNewBook => {// this.isFavourite = resNewBook.match;
        for (const result of resNewBook){
          this.favouriteUser = result.favourites;
          if (result.favourites.includes(this.post._id)) {
            this.isFavourite = true;
          }
        }
      });
  }
  // Send user data to the user profile component
  userProfile(user) {
    this.passDataService.sendUser(user);
    this.__router.navigate(['/user-profile']);
  }
  // Go back to previous location on cancel
  back() {
    this.location.back();
  }
}
