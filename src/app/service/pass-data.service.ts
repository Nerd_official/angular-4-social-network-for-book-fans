import { Injectable } from '@angular/core';
import {Subject} from "rxjs/Subject";
import {Observable} from "rxjs/Observable";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {Author} from "../Author";

@Injectable()
export class PassDataService {
  constructor() { }
  private subject = new Subject<any>();
  private messageSource = new BehaviorSubject<any>(Author);
  message = this.messageSource.asObservable();
  private userSource = new BehaviorSubject<any>(Author);
  user = this.userSource.asObservable();
  private logSource = new BehaviorSubject<boolean>(null);
  log = this.logSource.asObservable();
  private commentSource = new BehaviorSubject<any>(Comment);
  comment = this.commentSource.asObservable();
  // Log in data
  private adminSource = new BehaviorSubject<any>(Array);
  admin = this.adminSource.asObservable();
  sendMessage(message: string, length: number) {
    this.subject.next({name: message, value: length });
  }
  // Log in admin data
  sendAdmin(object: any) {
    this.adminSource.next(object);
  }
  // Send message data
  sendObject(object: any) {
    this.messageSource.next(object);
  }
  // Send user data
  sendUser(object: any) {
    this.userSource.next(object);
  }
  // Sends a boolean to check if user is logged in
  sendBool(data: boolean) {
    this.logSource.next(data);
  }
  // Send comment data
  sendComment(data: boolean) {
    this.commentSource.next(data);
  }
  clearMessage() {
    this.subject.next();
  }
  isLoggedIn(): Observable<any> {
   return this.logSource.asObservable();
 }
  getMessage(): Observable<any> {
    return this.messageSource.asObservable();
  }
  geUser(): Observable<any> {
    return this.userSource.asObservable();
  }
  getComment(): Observable<any> {
    return this.commentSource.asObservable();
  }

}

