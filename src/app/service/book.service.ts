import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import {Books} from '../Books';
import {Author} from '../Author';
import {User} from '../User';
import {Comment} from '../Comment';
@Injectable()
export class BookService {
  result: any;
  headers: any;
  options: any;
  userId: any;
  constructor(private _http: Http) {
    this.userId = localStorage.getItem('id');
    this.headers = new Headers();
    this.options = new RequestOptions({ headers: this.headers });
  }
  // server.js token
 getToken(data) {
   return this._http.get('/comments/' + data)
     .map(result => this.result = result.json());
 }
 // Check user is logged in
 checkUserDetails(user) {
   return this._http.post('/usercheck/', user, this.options)
     .map((response: Response) => this.result = response.json());
 }
  // Get all books
  getBooks() {
    return this._http.get('/books')
      .map(result => this.result = result.json());
  }
  // Get all comments
  getComments(book: Books) {
    return this._http.get('/getComments/' + book._id)
      .map(result => this.result = result.json());
  }
  // Save a comment
  saveComment(comment, id: Books) {
    return this._http.post('/comment/' + id._id , comment, this.options)
      .map((response: Response) => response.json());
  }
  // Save a favourite book
  saveFavourite(post: Books) {
    return this._http.post('/favourite/' + this.userId , post, this.options)
      .map((response: Response) => response.json());
  }
  // Remove ratings from user and books
  removeFavourite(post: Books) {
    return this._http.post('/remove_favourite/' + this.userId , post, this.options)
      .map((response: Response) => response.json());
  }
  // Check for favourite books
  isFavouriteBook(post: Books) {
    return this._http.post('/is_favourite/' + this.userId , post, this.options)
      .map(result => this.result = result.json());
  }
  // Get a users favourite books
  getCurrentUserFavouriteBooks(message) {
    return this._http.get('/getcurrentfavouritebooks/' + message._id)
      .map(result => this.result = result.json());
  }
  // Get all favourite users
  getAllFavouriteUsers(post: any) {
    return this._http.get('/getfavouriteusers/' + post)
      .map(result => this.result = result.json());
  }
  // Get user id
  getUserId(user) {
    return this._http.get('/getuserid/' + user)
      .map(result => this.result = result.json());
  }
  // Save book details
  saveBook(form, author: Author) {
    return this._http.post('/save/' + author._id , form, this.options)
      .map((response: Response) => response.json());
  }
  // Delete a book
  deleteBook(book: Books) {
    return this._http.delete('/delete/' + book._id )
      .map((response: Response) => response.json());
  }
  // Delete a book
  updateBooks(book: Books) {
    return this._http.put('/update/' + book._id, book)
      .map((response: Response) => response.json());
  }
  // Save author
  saveAuthor(form) {
    const newAuthor = new Author();
    newAuthor.firstName = form.get('firstName');
    newAuthor.lastName = form.get('lastName');
    return this._http.post('/addauthor', newAuthor, this.options)
      .map((response: Response) => response.json());
  }
  // Get all authors
  getAuthors() {
    return this._http.get('/getauthors')
      .map(result => this.result = result.json());
  }
  // Get Authors books
  getAuthorsBooks(form) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    const newBooks = new Books();
    newBooks.mypath = form.get('path');
    return this._http.post('/getauthorsbooks', newBooks, options)
      .map(result => this.result = result.json());
  }
  // Get the user rating for the current book
  getBookRating(book: Books) {
    return this._http.post('/getCurrentRating/' + this.userId, book, this.options)
      .map(result => this.result = result.json());
  }
  // Get all ratings
  getAllRatings(book: Books) {
    return this._http.get('/getRatings/' + book)
      .map(result => this.result = result.json());
  }
  // Save a rating
  saveRating(rating, id: Books) {
    const rateBook = new Books();
    rateBook._id = id._id;
    rateBook.rating = rating;
    return this._http.post('/rating/' + this.userId , rateBook, this.options)
      .map((response: Response) => response.json());
  }
  // Save admin book details
  saveAdminBook(form) {
    return this._http.post('/saveadminbook/' , form, this.options)
      .map((response: Response) => response.json());
  }
  /*
  *  Admin
  * */
  // Get all users - admin
  // Create admin
  createAdmin() {
    return this._http.get('/create_admin/')
      .map(result => this.result = result);
  }
  getAdminUsers() {
    return this._http.get('/admin_users/')
      .map((response: Response) => response.json());
  }
  // Save a user -admin
  saveAdminUser(user) {
    return this._http.post('/add_admin_user/' , user, this.options)
      .map((response: Response) => response.json());
  }
  // Update admin book details
  updateAdminUser(form, user) {
    return this._http.post('/update_admin_user/' + user._id , form, this.options)
      .map((response: Response) => response.json());
  }
  // Delete a user - admin
  deleteUser(user: User) {
    return this._http.delete('/delete_user/' + user._id )
      .map((response: Response) => response.json());
  }
  // Get all comments - admin
  getAllComments() {
    return this._http.get('/get_all_comments/')
      .map(result => this.result = result.json());
  }
  // Update user comments - admin
  updateUserComments(form, comment) {
    return this._http.post('/update_user_comment/' + comment._id , form, this.options)
      .map((response: Response) => response.json());
  }
  // Delete a comment - admin
  deleteComment(comment: Comment) {
    return this._http.delete('/delete_comment/' + comment._id )
      .map((response: Response) => response.json());
  }
  // Delete authors - admin
  deleteAuthor(author: Author) {
    return this._http.delete('/delete_author/' + author._id )
      .map((response: Response) => response.json());
  }
  // Update author - admin
  updateAuthor(form, author) {
    return this._http.post('/update_author/' + author._id , form, this.options)
      .map((response: Response) => response.json());
  }
}
