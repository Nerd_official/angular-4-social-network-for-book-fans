import {Component, Injectable} from '@angular/core';
import {Http, RequestOptions, Response, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';


@Injectable()
export class AuthService {
  result: any;
  public token: string;

  constructor(private http: Http) {
  }

  public isAuthenticated() {
    const token = localStorage.getItem('token');
    const headers = new Headers({'Authorization': 'Bearer ' + token});
    const options = new RequestOptions({headers: headers});
    return this.http.post('/auth', token, options)
      .map((response: Response) => {
        const result = response.json();
        if (result) {
          // return true to indicate successfull login
          return true;
        } else {
          // return false to indicate failed login
          return false;
        }
      });
  }

  // Verify user registration details
  saveUserRegisterDetails(data): Observable<any> {
    const headers = new Headers();
    const options = new RequestOptions({headers: headers});
    return this.http.post('/email', data, options)
      .map((response: Response) => {
        this.result = response.json();
        return this.result;
      });
  }
}
