import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import decode from 'jwt-decode';

@Injectable()
export class AdminGuardService implements CanActivate {
  tokenPayload: any;
  constructor(public router: Router) {}
  canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ): boolean {

    var token = localStorage.getItem('token');
    if (token !== null) {
        this.tokenPayload = decode(token);
    }
    if (token !== null && this.tokenPayload.role === 'admin') {
      return true;
    } else  {
      this.router.navigateByUrl('login');
      return false;
    }
  }
}
