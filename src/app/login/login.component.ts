import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import {BookService} from "../service/book.service";
import { Location } from '@angular/common';
import {Router} from "@angular/router";
import {PassDataService} from "../service/pass-data.service";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [BookService]
})
export class LoginComponent implements OnInit {
  model: any;
  errors: String;
  result: any;
  isLoggedIn = false;
  token: any;
  constructor(private bookService: BookService, private passDataService: PassDataService, private location: Location, private __router: Router) { }

  ngOnInit() {
    this.isLoggedIn = true;
  }
  // Log in submit button clicked
  onSubmitLogin (event) {
    this.errors = '';
    if (event.email === '' || event.password === '') {
      this.errors = "*Please enter Email Address and Password!"
    } else {
      this.bookService.checkUserDetails(event).subscribe(res => {
        this.result = res.user;
        var role = false;
        this.token = res.token;
        // Save current username and id to local storage
        localStorage.setItem('username', event.username);
        localStorage.setItem('id', res.id);
        if (res.role === 'admin') {
          role = true;
        }
        if (typeof this.result === 'object') {
          // Check for admin login and send to app component
          const loginCheck = [{login: true, admin: role}];
          this.passDataService.sendAdmin(loginCheck);
          // Save token
          localStorage.setItem('token', this.token);
          this.location.back();
        } else {
          this.errors = res.error;
        }
      });
    }
  }
  cancel() {
    this.location.back();
  }
  // Takes user to the register page
  register() {
    this.__router.navigate(['/register']);
  }
}

