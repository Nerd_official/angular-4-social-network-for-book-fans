/*
* Component that displays all books that are in the database
* Author Wayne Walker -wwalke02
* */
import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { FormsModule } from '@angular/forms';
import {BookService} from '../service/book.service';
import {Books} from '../Books';
import {Router} from "@angular/router";
import {PassDataService} from '../service/pass-data.service';
@Component({
  selector: 'app-send',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [BookService]
})
export class HomeComponent implements OnInit {
  books: Books;
  booksArray: Array<object>;
  constructor(private passDataService: PassDataService, private _bookService: BookService, private __router: Router) { }
  // Selected book object is passed to the comment component
  sendBook(event) {
    this.passDataService.sendObject(event);
    this.__router.navigate(['/comment']);
  }
  ngOnInit() {
    this.createAdmin();
    this._bookService.getBooks()
      .subscribe(
        res => {
          this.books = res;
          let newBook = [];
          let i = 0;
          let booksData: any;
          // Set book star ratings
          res.forEach(function(element) {
            booksData = {
              '_id':  element._id,
              'firstName':  element.firstName,
              'lastName': element.lastName,
              'url': element.url,
              'title': element.title,
              'checked': this.getCheckedVal(1, Math.round(element.rating)),
              'checked1': this.getCheckedVal(2, Math.round(element.rating)),
              'checked2': this.getCheckedVal(3, Math.round(element.rating)),
              'checked3': this.getCheckedVal(4, Math.round(element.rating)),
              'checked4': this.getCheckedVal(5, Math.round(element.rating))};
            i++;
            newBook.push(booksData);
          }.bind(this));
          this.booksArray = newBook;
        });
  }
  // Returns a response that depends on value for star rating display
  getCheckedVal(checked, val) {
    if (checked <= val) {
      return 'checked';
    }
    return '';
  }
  // Create a admin account -> admin/ password: 1234
  createAdmin() {
    this._bookService.createAdmin()
      .subscribe(
        res => {console.log("create admin" + res);
    });
  }
}
