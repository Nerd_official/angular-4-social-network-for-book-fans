var mongoose = require("mongoose");
const Schema = mongoose.Schema;

var commentSchema = new Schema({
  name: String,
  userId: String,
  title: String,
  comments: String,
  rating: Number,
  created : {
    type : Date,
    default : Date.now
  }
});

// Create a model based on the schema
module.exports = mongoose.model('Comments', commentSchema, 'comment');
