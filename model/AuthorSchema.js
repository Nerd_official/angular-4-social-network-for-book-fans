var mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create a schema
var authorSchema = new Schema({
  firstName: String,
  lastName: String,
  books: [{type: Schema.ObjectId, ref: 'Books'}]
});
// Create a model based on the schema
module.exports = mongoose.model('Author', authorSchema, 'author');

