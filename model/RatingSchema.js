var mongoose = require("mongoose");
const Schema = mongoose.Schema;

var ratingSchema = new Schema({
  bookId: String,
  userId: String,
  rating: Number,
});
// Create a model based on the schema
module.exports = mongoose.model('Rate', ratingSchema, 'rate');
