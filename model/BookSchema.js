var mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create a schema

var bookSchema = new Schema({
  firstName: String,
  lastName: String,
  path: String,
  title: String,
  url: String,
  comments: [{type: Schema.ObjectId, ref: 'Comments'}],
  favourite: [{type: Schema.ObjectId, ref: 'User'}],
  rating: Number
});
// Create a model based on the schema
module.exports = mongoose.model('Books', bookSchema, 'book');

