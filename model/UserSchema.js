var mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create a schema

var userSchema = new Schema({
  userName: String,
  password: String,
  email: String,
  role: String,
  favourites : [{ type: Schema.ObjectId, ref: 'Books' }],
  rating : [{ type: Schema.ObjectId, ref: 'Rate' }]
});
userSchema.methods.comparePassword = function(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
    if (err) return cb(err);
    cb(null, isMatch);
  });
};
module.exports = mongoose.model('User', userSchema, 'user');

