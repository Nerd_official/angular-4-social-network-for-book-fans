const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const http = require('http');
const routes = require('./routes/routes');
const port = 3000;

const app = express();

app.use(express.static(path.join(__dirname, 'dist')));

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(express.static('uploads'));
app.use('/', routes);
// Check if token is valid to view private pages
app.post('/auth', verifyToken, function(req, res) {
  //console.log('verify start');
  const jwt = require('jsonwebtoken');
  jwt.verify(req.token, 'secretkey', function(err, authData){
    if(err) {
      console.log('forbidden');
      res.send(false);
    } else {
      res.send(true);
    }
  });
});
// Verify Token
function verifyToken(req, res, next) {
  // Get auth header value
  const bearerHeader = req.headers['authorization'];
  // Check if bearer is undefined
  if(typeof bearerHeader !== 'undefined') {
    // Split at the space
    const bearer = bearerHeader.split(' ');
    // Get token from array
    const bearerToken = bearer[1];
    // Set the token
    req.token = bearerToken;
    // Next middleware
    next();
  } else {
    // Forbidden
   // console.log('verfiry forbidden');
    res.send(false);
  }

}
app.get('*', function(req, res){
  res.sendFile(path.join(__dirname, 'dist/index.html'));
});

app.set('port', port);

const server = http.createServer(app);

server.listen(port, function() {
  console.log('Running on localhost 3000')
});
