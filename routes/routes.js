var express = require('express');
const router = express.Router();
const multer = require('multer');
const uploads = multer({ dest: './uploads/'});
const books = require('../model/BookSchema');
const comments = require('../model/CommentSchema');
const author = require('../model/AuthorSchema');
const User = require('../model/UserSchema');
const rates = require('../model/RatingSchema');
bcrypt = require('bcryptjs');
//load mongoose package
const mongoose = require('mongoose');
const nev = require('email-verification')(mongoose);
var fs = require('fs');
//Route controllers
var adminController = require('../routes/admin');
var commentController = require('../routes/comments');
var booksController = require('../routes/books');
var ratingController = require('../routes/ratings');
var authorController = require('../routes/authors');
var favouriteController = require('../routes/favourite');


// Use native Node promises
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/booklist')
  .then(function() {
    console.log('mongo set up');
  });
// sync version of hashing function
var myHasher = function(password, tempUserData, insertTempUser, callback) {
  var hash = bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
  return insertTempUser(hash, tempUserData, callback);
};
// NEV configuration ===================== : https://www.npmjs.com/package/email-verification
nev.configure({
  persistentUserModel: User,
  expirationTime: 6000, // 10 minutes

  verificationURL: 'http://localhost:3000/email-verification/${URL}',
  transportOptions: {
    service: 'Gmail',
    auth: {
      user: 'xxxxx',
      pass: 'xxxx'
    }
  },

  hashingFunction: myHasher,
  passwordFieldName: 'password'
}, function(err, options) {
  if (err) {
    console.log(err);
    return;
  }

  console.log('configured: ' + (typeof options === 'object'));
});

nev.generateTempUserModel(User, function(err, tempUserModel) {
  if (err) {
    console.log(err);
    return;
  }

  console.log('generated temp user model: ' + (typeof tempUserModel === 'function'));
});
// Boiler plate code provider by library : https://www.npmjs.com/package/email-verification
router.post('/email', function(req, res) {
  var email = req.body.email;

  // register button was clicked
    //if (req.body.type === 'register') {
    var password = req.body.password;
    var username = req.body.username;
    var newUser = new User({
      userName: username,
      email: email,
      password: password,
      confirmed: true
    });

    nev.createTempUser(newUser, function(err, existingPersistentUser, newTempUser) {
      if (err) {
        return res.json({msg: 'ERROR: creating temp user FAILED'});
      }

      // user already exists in persistent collection
      if (existingPersistentUser) {
        return res.json({
          msg: 'You have already signed up and confirmed your account. Did you forget your password?'
        });
      }

      // new user created
      if (newTempUser) {
        var URL = newTempUser[nev.options.URLFieldName];
        nev.sendVerificationEmail(email, URL, function(err, info) {
          if (err) {
            return res.json({msg: 'ERROR: sending verification email FAILED'});
          }
          res.json({
            msg: 'An email has been sent to you. Please check it to verify your account.',
            info: info
          });
        });

        // user already exists in temporary collection!
      } else {
        res.json({
          msg: 'You have already signed up. Please check your email to verify your account.'
        });
      }
    });
});
// User accesses the link that is sent : https://www.npmjs.com/package/email-verification
router.get('/email-verification/:URL', function(req, res) {
  var url = req.params.URL;

  nev.confirmTempUser(url, function(err, user) {
    if (user) {
      nev.sendConfirmationEmail(user.email, function(err, info) {
        if (err) {
          return res.json({msg: 'ERROR: sending confirmation email FAILED'});
        }
        res.redirect('/home');
      });
    } else {
      return res.json({msg: 'ERROR: confirming temp user FAILED'});
    }
  });
});
// Check a user`s password and username on log in
router.post('/usercheck', function(req, res) {
  var test = "";
  var token = "";
  User.findOne({'userName': req.body.username}, { _id: 1, userName: 1, password: 1, email: 1, role: 1} )
    .exec(function(err, user) {
      if (err) {
        console.log('Error checking user');
      } else if(user) {
        user.comparePassword(req.body.password, function(err, isMatch) {
          if (err) throw err;
          console.log('user check error ' + err);
          if (isMatch) {
            const jwt = require('jsonwebtoken');
            // Mock user
            const user1 = {
              _id: user._id,
              username: user.userName,
              email:user.email,
              role: user.role
            }
            jwt.sign(user1, 'secretkey', { expiresIn: '72h' }, function(err, token) {
              res.json({username: user1.username, user: user1, id: user1._id, token: token, role: user1.role});
            });

          } else {
            res.json({error: '*Password is wrong'});
          }
        });
      } else {
        res.json({error: '*Username not found'});
      }
    });
});
//Get user id
router.get('/getuserid/:id', function(req, res) {
  User.find({userName: req.params.id}, {password: 0}, {email: 0}).populate(
    'favourites')
    .exec(function(err, user) {
      if (err) {
        console.log('Error getting favourite books' + err);
      } else {
        res.json(user);
      }
    });
});

/*
* Comments
* */
//Get the list of comments
router.get('/getComments/:id', commentController.getComments);

// Save comments to a book
router.post('/comment/:id', commentController.save_comment);

/*
* Favourites
* */
// Add favourite book
router.post('/favourite/:id', favouriteController.addFavouriteBook);
//Remove favourite book
router.post('/remove_favourite/:id', favouriteController.removeFavouriteBook);
//Check if a users favourite book exists
router.post('/is_favourite/:id', favouriteController.isFavouriteBook);
//Get the current user favourite books
router.get('/getcurrentfavouritebooks/:id', favouriteController.getCurrentFavouriteBook);
//Get all users who have favourited a book
router.get('/getfavouriteusers/:id', favouriteController.getFavouriteUsers);

/*
* Authors
* */
//Add author to database
router.post('/addauthor', authorController.addAuthor);
//Get all authors
router.get('/getauthors', authorController.getAuthors);
//Get the list of books written by a author
router.post('/getauthorsbooks', authorController.getAuthorBooks);

/*
* Books
* */
// Save a book
router.post('/save/:img', uploads.any(), booksController.savebook);
// Delete a book
router.delete('/delete/:id', booksController.deleteBook);
// Update a book
router.put('/update/:id', booksController.updateBook);
// Find a book
router.get('/books/:id', booksController.findBook);
// Get all books
router.get('/books', booksController.getAllBooks);

/*
* Ratings
* */
//Get the current user rating for a book
router.post('/getCurrentRating/:id', ratingController.getCurrentRating);
// Save the users current book rating
router.post('/rating/:id', ratingController.saveBookRating);

/*
* Admin
* */
// Save books - admin
router.post('/saveadminbook', uploads.any(), adminController.saveadminbook);
// Find all user - admin
router.get('/admin_users', adminController.admin_users);
//Add user to database - admin
router.post('/add_admin_user', adminController.add_admin_user);
// Update user to database - admin
router.post('/update_admin_user/:id', adminController.update_admin_user);
// Delete a user - admin
router.delete('/delete_user/:id', adminController.delete_user);
//Get all comments - admin
router.get('/get_all_comments', adminController.get_all_comments);
// Update user comments - admin
router.post('/update_user_comment/:id', adminController.update_user_comment);
// Update authors - admin
router.post('/update_author/:id', adminController.update_author);
// Delete author - admin
router.delete('/delete_author/:id', adminController.delete_author);
// Delete a comment - admin
router.delete('/delete_comment/:id', adminController.delete_comment);
// Create admin account
router.get('/create_admin', adminController.create_admin);
module.exports = router;

