const author = require('../model/AuthorSchema');

//Add author to database
exports.addAuthor = function(req, res) {
  var newAuthor = new author();
  newAuthor.firstName = req.body.firstName;
  newAuthor.lastName = req.body.lastName;
  newAuthor.save(newAuthor,
    function(err, added) {
      if(err) {
        res.send("Error saving author");
      } else {
        res.json(added)
      }
    }
  )
};
//Get all authors
exports.getAuthors = function(req, res) {
  author.find({})
    .exec(function(err, authors) {
      if (err) {
        console.log('Error getting authors' + err);
      } else {
        res.json(authors);
      }
    });
};
//Get the list of books written by a author
exports.getAuthorBooks = function(req, res) {
  author.find({_id: req.body.mypath}).populate(
    'books')
    .exec(function(err, books) {
      if (err) {
        console.log('Error getting author books' + err);
      } else {
        res.json(books[0].books);
      }
    });
};
