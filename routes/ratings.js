const books = require('../model/BookSchema');
const User = require('../model/UserSchema');
const rates = require('../model/RatingSchema');

//Get the current user rating for a book
exports.getCurrentRating = function(req, res) {
  User.find({_id: req.params.id}, {rating: 1}).populate({
    path: 'rating',
    rating: {$in: [req.body._id]}

  })
    .exec(function (err, user) {
      if (err) {
        console.log("is rating book error" + err);
        // res.json({match: 'false'})
      } else {
        res.json(user);
      }
    });
};
// Save the users current book rating
exports.saveBookRating = function(req, res) {
  rates.findOneAndUpdate({ $and: [ {bookId : req.body._id}, {userId : req.params.id}]}, {$set:{rating: req.body.rating}}, {
    new: true
  }, function (err, rate) {
    if (err){
      console.log("rting err" + err);
    }
    if (rate === null) {
      var rateBook = new rates();
      rateBook.bookId = req.body._id;
      rateBook.userId = req.params.id;
      rateBook.rating = req.body.rating;
      rateBook.save(
        function(err, added) {
          if(err) {
            res.send({'error': "Error saving ratings"});
          } else {
            console.log("rate book saved" + added);
          }
        });
      // Save ratings to the user table
      User.findByIdAndUpdate(req.params.id,
        {
          $addToSet: {rating: rateBook}
        },
        {
          new: true
        },
        function(err, ratings) {
          if(err) {
            // res.send("Error saving user ratings");
          } else {

          }
          res.json(ratings)
          saveRatings(req.body._id);
        });

    } else {
      res.json({msg: saveRatings(req.body._id)});
    }
  });
};
// Calculate and save the average rating for a book
function saveRatings(req) {
  var total = 0;
  var countSize = 0;
  rates.find({ bookId: req},'rating bookId')
    .exec(function (err, rate) {
      if (err) {
        console.log("its a rating book error" + err);
        // res.json({match: 'false'})
      } else {

        rate.forEach(function(res, index) {
          total += res.rating
          countSize ++;
        });
        books.findByIdAndUpdate(req,
          {
            $set: { rating: total/countSize }
          },
          {
            new: true
          },
          function(err, ratings) {
            if(err) {
              // res.send("Error saving user ratings");
            } else {
             // res.json(ratings)
            }
               return ratings;
          });
        // res.json(user);
      }
    });
}
