const books = require('../model/BookSchema');
const comments = require('../model/CommentSchema');
const author = require('../model/AuthorSchema');
const User = require('../model/UserSchema');
var fs = require('fs');

// Save books - admin
exports.saveadminbook = function(req, res){
  console.log("save admin book called ");
  var newBook = new books();
  if (req.files) {
    newBook.url = 'img-not-available.png';
    req.files.forEach(function (file) {
      var filename = (new Date).valueOf() + "-" + file.originalname;
      newBook.url = filename;
      fs.rename(file.path, './uploads/' + filename, function (err) {
        if (err) throw err;
      });
    });
  } else {
    newBook.url = 'uploads/img-not-available.png';
  }
  newBook.firstName = req.body.firstName;
  newBook.lastName = req.body.lastName;
  newBook.title = req.body.title;
  newBook.save(function(err,book){
    if (err) {
      console.log(err);
    } else {
      author.findOneAndUpdate({  $and:[ { $or : [ { firstName : req.body.firstName }] },
          { $or : [ { lastName : req.body.lastName }]}]},
        {
          $addToSet: {books: newBook}
        },
        {upsert: true},
        function(err, saveBook) {
          if (err) {
            console.log("Error uploading" + err)
          }
          console.log("Success saving book-admin")
        });
      res.json(book);
    }
  });
};

// Find all user - admin
exports.admin_users = function(req, res) {
  console.log("admin user all called in routes");
  User.find({})
    .exec(function(err, user) {
      if (err) {
        console.log('Error getting user in admin' + err);
      } else {
        res.json(user);
      }
    });
};

//Add user to database - admin
exports.add_admin_user = function(req, res) {
  console.log("add admin user" + req.body.password);
  var email = req.body.email;
  var password = req.body.password;
  var username = req.body.userName;
  if( username === 'admin') {
    var role = 'admin';
  } else {
    role = 'user';
  }
  password = bcrypt.hashSync(req.body.password, 8);
  var newUser = new User({
    userName: username,
    email: email,
    password: password,
    role: role
  });
  newUser.save(function(err, userAdded) {
      if(err) {
        res.send("Error saving user" + err);
      } else {
        res.json(userAdded)
      }
    }
  )
};
// Update user to database - admin
exports.update_admin_user = function(req, res) {
  var password = '';
  if (req.body.password.length <= 12) {
    password = bcrypt.hashSync(req.body.password, 8);
  } else {
    password = req.body.password;
  }
  User.findByIdAndUpdate(req.params.id,
    {
      $set: {userName: req.body.userName, email: req.body.email, password: password}
    },
    {
      new: true
    },
    function(err, updateBook) {
      if(err) {
        res.send("Error updating user " + err);
      } else {
        res.json(updateBook)
      }
    });
};
// Delete a user - admin
exports.delete_user = function(req, res) {
  User.findByIdAndRemove(req.params.id,
    function(err, deleteUser) {
      if(err) {
        res.send("Error deleting user" + err);
      } else {
        res.json(deleteUser)
      }
    });
};
//Get all comments - admin
exports.get_all_comments = function(req, res) {
  comments.find({})
    .exec(function(err, comments) {
      if (err) {
        console.log('Error getting comments admin' + err);
      } else {
        res.json(comments);
      }
    });
};
// Update user comments - admin
exports.update_user_comment = function(req, res) {
  comments.findByIdAndUpdate(req.params.id,
    {
      $set: {comments: req.body.comments, title: req.body.title, name:  req.body.name, created:  req.body.created}
    },
    {
      new: true
    },
    function(err, updateComment) {
      if(err) {
        res.send("Error updating comment");
      } else {
        res.json(updateComment)
      }
    });
};
// Update authors - admin
exports.update_author = function(req, res) {
  author.findByIdAndUpdate(req.params.id,
    {
      $set: {firstName: req.body.firstName, lastName: req.body.lastName}
    },
    {
      new: true
    },
    function(err, updateAuthor) {
      if(err) {
        res.send("Error updating Author");
      } else {
        res.json(updateAuthor)
      }
    });
};
// Delete author - admin
exports.delete_author = function(req, res) {
  console.log("Delete author" + req.params.id);
  author.findByIdAndRemove(req.params.id,
    function(err, deleteAuthor) {
      if(err) {
        res.send("Error deleting Author");
      } else {
        res.json(deleteAuthor)
      }
    });
};
// Delete a comment - admin
exports.delete_comment = function(req, res) {
  console.log("Delete user" + req.params.id);
  comments.findByIdAndRemove(req.params.id,
    function(err, deleteComment) {
      if(err) {
        res.send("Error deleting Comment");
      } else {
        res.json(deleteComment)
      }
    });
};
// Create admin account
exports.create_admin = function(req, res) {
  var password = '';
  password = bcrypt.hashSync('1234', 8);
  User.findOneAndUpdate({userName: 'admin'},
    {
      $set: {userName: 'admin', email: 'test@hotmail.com', password:  password, role:  'admin'}
    }, {upsert: true},
    function(err, createAdmin) {
      if(err) {
        console.log("update " + err);
        //res.send("Error updating comment");
      } else {
        res.send(createAdmin);
      }
    });
};
//module.exports = router;


