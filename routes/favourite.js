const books = require('../model/BookSchema');
const User = require('../model/UserSchema');

// Add favourite book
exports.addFavouriteBook = function(req, res) {
  books.findByIdAndUpdate(req.body._id,
    {
      $addToSet: {favourite: req.params.id}
    },
    {
      new: true
    },
    function(err, user) {
      if(err) {
        res.send("Error saving book favorite" + err);
      } else {

      }
      //res.json(user);
    });
  //save favourite in user table
  User.findByIdAndUpdate(req.params.id,
    {
      $addToSet: {favourites: req.body._id}
    },
    {
      new: true
    },
    function(err, favourite) {
      if(err) {
        res.send("Error saving user favourite" + err);
      } else {

      }
      res.json(favourite)
    });
};
//Remove favourite book
exports.removeFavouriteBook = function(req, res) {
  books.findByIdAndUpdate(req.body._id,
    {
      $pull: {favourite: req.params.id}
    },
    {
      new: true
    },
    function(err, favourite) {
      if(err) {
        res.send("Error removing user favourite" + err);
      } else {

      }
      //.json(ratings)
    });
  //Remove favourite book from user
  User.findByIdAndUpdate(req.params.id,
    {
      $pull: {favourites: req.body._id}
    },
    {
      new: true
    },
    function(err, favourite) {
      if(err) {
        res.send("Error saving user favourite");
      } else {

      }
      //res.json(ratings)
    });
  books.findByIdAndUpdate(req.body._id,
    {
      $pull: {favourites: req.params.id}
    },
    {
      new: true
    },
    function(err, favourite) {
      if(err) {
        res.send("Error saving user favourite");
      } else {

      }
      res.json(favourite)
    });
};
//Check if a users favourite book exists
exports.isFavouriteBook = function(req, res) {
  User.find({_id: req.params.id}, {favourites: 1}).populate({
    path: 'favourite',
    favourites: {$in: [req.body._id]},
  })
    .exec(function (err, user) {
      if (err) {
        console.log("is favourites book error" + err);
        // res.json({match: 'false'})
      } else {
        res.json(user);
      }
    });
};
//Get the current user favourite books
exports.getCurrentFavouriteBook = function(req, res) {
  User.find({_id: req.params.id}, {favourites: 1}).populate(
    'favourites')
    .exec(function(err, books) {
      if (err) {
        console.log('Error getting favourite books' + err);
      } else {
        res.json(books);
      }
    });
};
//Get all users who have favourited a book
exports.getFavouriteUsers = function(req, res) {
  books.find({_id: req.params.id}, {favourites: 1}).populate({
    path: 'favourite',
    select: 'userName'
  })
    .exec(function (err, user) {
      if (err) {
        console.log("is favourites users book error" + err);
        // res.json({match: 'false'})
      } else {
        res.json(user);
      }
    });
};
