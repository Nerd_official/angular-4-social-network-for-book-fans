const comments = require('../model/CommentSchema');
const books = require('../model/BookSchema');

// Save comments to a book
exports.save_comment = function(req, res) {
  var comm = new comments();
  var date = new Date();
  var str = date;
  comm.userId = req.params.id;
  comm.name = req.body.name;
  comm.title = req.body.title;
  comm.comments = req.body.comment;
  comm.created = str;
  comm.save(
    function(err, added) {
      if(err) {
        res.send("Error saving comments" + err);
      } else {
      }
    });
  books.findByIdAndUpdate(req.params.id,
    {
      $addToSet: {comments: comm}
    },
    {
      new: true
    },
    function(err, comments) {
      if(err) {
        res.send("Error saving comments");
      } else {

      }
      res.json(comments)
    });
};
//Get the list of comments
exports.getComments = function(req, res) {
  books.find({_id: req.params.id}).populate(//[
    'comments')
    .exec(function(err, author) {
      if (err) {
        console.log('Error getting comments' + err);
      } else {
        res.json(author[0].comments);
      }
    });
};
