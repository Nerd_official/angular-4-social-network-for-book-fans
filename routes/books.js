const books = require('../model/BookSchema');
const author = require('../model/AuthorSchema');
var fs = require('fs');

// Save a book
exports.savebook = function(req, res) {
  var newBook = new books();
  if (req.files) {
    newBook.url = 'img-not-available.png';
    req.files.forEach(function (file) {
      var filename = (new Date).valueOf() + "-" + file.originalname;
      newBook.url = filename;
      fs.rename(file.path, './uploads/' + filename, function (err) {
        if (err) throw err;
      });
    });

  } else {
    // https://www.google.co.uk/search?q=image+not+available+png&tbm=isch&tbo=u&source=univ&sa=X&ved=0ahUKEwjJ8hofluPPZAhVLBcAKHZffD8kQ7AkINA&biw=1366&bih=662#imgrc=dhO1SE9fkIkdjM:
    newBook.url = 'uploads/img-not-available.png';
  }
  newBook.firstName = req.body.firstName;
  newBook.lastName = req.body.lastName;
  newBook.title = req.body.title;
  newBook.save(function(err,book){
    if (err) {
      console.log(err);
    } else {
      var author1 = new author();
      console.log("Route nebook " + req.params.id);
      var test1 = req.params.img;
      author.findByIdAndUpdate(req.params.img,
        {
          $addToSet: {books: newBook}
        },
        {
          new: true
        },
        function(err, saveBook) {
          if (err) {
            console.log("Error uploading" + err)
          } else {
            console.log("Success uploading")
          }
        });
      res.json(book);
    }
  });
};
// Delete a book
exports.deleteBook = function(req, res){
  console.log("Delete book" + req.params.id);
  books.findByIdAndRemove(req.params.id,
    function(err, deleteBook) {
      if(err) {
        res.send("Error deleting book");
      } else {
        res.json(deleteBook)
      }
    });
};
// Update a book
exports.updateBook = function(req, res){
  books.findByIdAndUpdate(req.params.id,
    {
      $set: {title: req.body.title}
    },
    {
      new: true
    },
    function(err, updateBook) {
      if(err) {
        res.send("Error deleting book");
      } else {
        res.json(updateBook)
      }
    });
};
// Find a book
exports.findBook = function(req, res) {
  const id = req.params.id;
  books.findById(id)
    .exec(function(err, book) {
      if (err) {
        console.log('Error finding book' + err);
      } else {
        res.json(book);
      }
    });
};
// Get all books
exports.getAllBooks = function(req, res) {
  books.find({})
    .exec(function(err, books) {
      if (err) {
        console.log('Error getting books' + err);
      } else {
        res.json(books);
      }
    });
};
